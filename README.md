# omnistack8

semana 8 curso rocketseat

#Criando pasta backend
mkdir  backend  && cd backend
#executando 
yarn init -y

#instalar express
# micro framework que ajuda a lidar com rotas
yarn add express

#Instalando o Nodemon apenas para desenvolvimento
yarn add nodemon -D

#connect monogo
mongodb+srv://<username>:<password>@cluster0-4obsk.mongodb.net/test?retryWrites=true&w=majority

#Instalar mongoose
yarn add mongoosere

#SDK
export ANDROID_HOME=/home/mauro/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools

#no ios usar o cocoapods só no mac
#rodar po
yarn add react-navigation react-native-gesture-handler react-native-reanimated
#pod install
#vai instalar as depedencias no ios criando o autolink

#bala de prata
adb reverse tcp:3333 tcp:3333