import React, {useState} from 'react';
import './Login.css'
import api from '../services/api'

import logo from '../assets/logo.svg';

export default function Login({history}) {
    const [username, setUsername] = useState('')
  
    async function handleSubmit(e){
        //previne comportamento default de enviar para outra página
        e.preventDefault()

        const response = await api.post('devs/',{
            username
        })
        console.log(response.data)
        const {_id} = response.data
        history.push(`/dev/${_id}`)
    }

    return (
        <div className="login-container">
            <form onSubmit={handleSubmit}>
                <img src={logo} alt="Tindev"/>
                <input 
                    placeholder="Digite seu usuário no GitHub" 
                    value={username} 
                    onChange={e => setUsername(e.target.value)}/>
                <button type="submit">Enviares</button>
            </form>

        </div> 

    );
}
